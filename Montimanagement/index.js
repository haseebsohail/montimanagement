/**
 * @format
 */

 import {AppRegistry} from 'react-native';
 import {LanguageContextProvider} from './src/LanguageContext';
 import {name as appName} from './app.json';
 
 AppRegistry.registerComponent(appName, () => LanguageContextProvider);
 