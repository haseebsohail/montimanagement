import {Constants} from '../Theme';

const axios = require('axios');
const XMLParser = require('react-xml-parser');

const CC_GUID = '32bbfc73-01a4-4795-ad71-141246b79c25';

const soapXMLMain = body => {
  return `<?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
      <soap:Body>
      ${body}
      </soap:Body>
    </soap:Envelope>`;
};

const axiosCall = (xmlFunction, myCallback) => {
  const xmls = soapXMLMain(xmlFunction);
  // console.log('xmls', xmls);
  const returnData = {
    success: false,
    responseBody: null,
  };
  axios
    .post(Constants.API, xmls, {
      headers: {'Content-Type': 'text/xml; charset=utf-8'},
    })
    .then(res => {
      // console.log('Response', JSON.stringify(res.data));
      var json = new XMLParser().parseFromString(res.data);
      returnData.success = true;
      returnData.responseBody = json.children[0].children[0].children[0];
      myCallback(returnData);
    })
    .catch(err => {
      // console.log('error', err);
      returnData.success = false;
      returnData.responseBody = err;
      myCallback(returnData);
    });
};

export const LoginApi = (body, myCallback) => {
  const xmlFunction = `<fnLogin xmlns="http://tempuri.org/">
    <COMP_CODE>${body.companyCode}</COMP_CODE>
    <USERNAME>${body.userName}</USERNAME>
    <PASSWORD>${body.password}</PASSWORD>
  </fnLogin>`;

  axiosCall(xmlFunction, myCallback);
};

export const SignupAPI = (body, myCallback) => {
  const xmlFunction = `<RegisterCustomer xmlns="http://tempuri.org/">\
      <Phone>${body.phoneNumber}</Phone>\
      <CC_GUID>${CC_GUID}</CC_GUID>\
    </RegisterCustomer>`;

  axiosCall(xmlFunction, myCallback);
};

export async function getCustomerAPI(body, myCallback) {
  const login_name = body.phoneNumber;
  const xmlFunction = `<getCustomer xmlns="http://tempuri.org/">\
    <strLoginID>${login_name}</strLoginID>\
  </getCustomer>`;
  axiosCall(xmlFunction, myCallback);
}

export async function CMB_AgentBranchesAPI(agent_GUID, myCallback) {
  // const AgentID_GUID = '32bbfc73-01a4-4795-ad71-141246b79c25'; //FOr testing
  const xmlFunction = `<CMB_AgentBranches xmlns="http://tempuri.org/">>
    <CC_GUID>${agent_GUID}</CC_GUID>
  </CMB_AgentBranches>`;
  axiosCall(xmlFunction, myCallback);
}

export async function CMB_CurrencyAPI(myCallback) {
  const xmlFunction = '<CMB_Currency xmlns="http://tempuri.org/" />';
  axiosCall(xmlFunction, myCallback);
}

export async function GetCurrencyCorrespondentAPI(body, myCallback) {
  const xmlFunction = `<fnGetCurrencyCorrespondent xmlns="http://tempuri.org/">
    <CCID>${body.CCID}</CCID>
    <CCYID>${body.CCYID}</CCYID>
    <strEnd>${body.strEnd}</strEnd>
  </fnGetCurrencyCorrespondent>`;
  axiosCall(xmlFunction, myCallback);
}

export async function GetCurrencyStockAPI(body, myCallback) {
  const xmlFunction = `<fnGetCurrencyStock xmlns="http://tempuri.org/">
    <CCID>${body.CCID}</CCID>
    <strEnd>${body.strEnd}</strEnd>
  </fnGetCurrencyStock>`;
  axiosCall(xmlFunction, myCallback);
}

export async function fnGetRoRiTotal(body, myCallback) {
  const xmlFunction = `<fnGetRoRiTotal xmlns="http://tempuri.org/">
      <strFrom>${body.strFrom}</strFrom>
      <strTo>${body.strTo}</strTo>
      <CustID_GID></CustID_GID>
    </fnGetRoRiTotal>`;
  axiosCall(xmlFunction, myCallback);
}

export async function GetCurrencyExposureAPI(body, myCallback) {
  const xmlFunction = `<fnGetCurrencyExposure xmlns="http://tempuri.org/">
    <CCID>${body.CCID}</CCID>
    <strEnd>${body.strEnd}</strEnd>
    </fnGetCurrencyExposure>`;
  axiosCall(xmlFunction, myCallback);
}

export async function GetFCYLedgerAPI(body, myCallback) {
  const xmlFunction = `<fnGetFCYLedger xmlns="http://tempuri.org/">
    <CCYID>${body.CCYID}</CCYID>
    <strDTFrom>${body.strDTFrom}</strDTFrom>
    <strDTto>${body.strDTto}</strDTto>
    <ACCNo>${body.ACCNo}</ACCNo>
  </fnGetFCYLedger>`;
  axiosCall(xmlFunction, myCallback);
}

export async function GetROListing(body, myCallback) {
  const xmlFunction = `<fnGetROListing xmlns="http://tempuri.org/">
    <CCID>${body.CCID}</CCID>
    <strFrom>${body.strFrom}</strFrom>
    <strTo>${body.strTo}</strTo>
  </fnGetROListing>`;
  axiosCall(xmlFunction, myCallback);
}

export async function GetRIListing(body, myCallback) {
  const xmlFunction = `<fnGetRIListing xmlns="http://tempuri.org/">
    <CCID>${body.CCID}</CCID>
    <strFrom>${body.strFrom}</strFrom>
    <strTo>${body.strTo}</strTo>
  </fnGetRIListing>`;
  axiosCall(xmlFunction, myCallback);
}
