import React, {useEffect, useState} from 'react';
import {ImageBackground, Image} from 'react-native';
import {connect} from 'react-redux';

import messaging from '@react-native-firebase/messaging';
import PushNotification from 'react-native-push-notification';

import {Images} from '../../Theme';
import {LoginApi, getCustomerAPI} from '../../Store/apiCalls';
import {AsyncStorage} from '../../Theme/Libraries';
import styles from './style';

const SplashScreen = props => {
  const [user, setUser] = useState('00923146625910');
  const [fcmToken, setFcmToken] = useState();


  // const checkPermission = async () => {
  //   console.log('checking firebase messaging permission');
  //   const hasPermission = await messaging().hasPermission();
  //   console.log('Permission issue', hasPermission);
  //   let token = null;
  //   if (hasPermission) {
  //     try {
  //       token = await messaging().getToken();
  //       setFcmToken(token);
  //     } catch (error) {
  //       console.log(error);
  //     }
  //     console.log('Token', token);
  //   }
  // };

  // const pushNotif = () => {
  //   PushNotification.configure({
  //     // (optional) Called when Token is generated (iOS and Android)
  //     onRegister: function (token) {
  //       console.log('TOKEN:', token);
  //     },

  //     // (required) Called when a remote is received or opened, or local notification is opened
  //     onNotification: function (notification) {
  //       console.log('NOTIFICATION:', notification);

  //       // process the notification

  //       // (required) Called when a remote is received or opened, or local notification is opened
  //       // notification.finish(PushNotificationIOS.FetchResult.NoData);
  //     },

  //     // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
  //     onAction: function (notification) {
  //       console.log('ACTION:', notification.action);
  //       console.log('NOTIFICATION:', notification);

  //       // process the action
  //     },

  //     // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
  //     onRegistrationError: function (err) {
  //       console.error(err.message, err);
  //     },

  //     // IOS ONLY (optional): default: all - Permissions to register.
  //     permissions: {
  //       alert: true,
  //       badge: true,
  //       sound: true,
  //     },

  //     // Should the initial notification be popped automatically
  //     // default: true
  //     popInitialNotification: true,

  //     /**
  //      * (optional) default: true
  //      * - Specified if permissions (ios) and token (android and ios) will requested or not,
  //      * - if not, you must call PushNotificationsHandler.requestPermissions() later
  //      * - if you are not using remote notification or do not have Firebase installed, use this:
  //      *     requestPermissions: Platform.OS === 'ios'
  //      */
  //     requestPermissions: true,
  //   });
  // };

  const responseFunction = async res => {

    if (!res.success) {
      props.navigation.navigate('LoginScreen', {
        token: fcmToken ? fcmToken : 'No token ',
      });
    } else {
      let data = res.responseBody.children;
      if (data) {
        let USER_DETAIL = {};
        data.forEach(element => {
          USER_DETAIL[element.name] = element.value;
        });

        if (USER_DETAIL.isError === 'True') {
          alert(USER_DETAIL.Result);
        } else {
          props.dispatch({type: 'USER_DETAIL', USER_DETAIL});
          props.navigation.navigate('HomeScreen');
        }
      }
    }
   
  };

  React.useEffect(() => {
    const timeout = setTimeout(async () => {
      const userDetail = await AsyncStorage.getItem('@credientials');
      console.log({SplashScreen_userDetail: userDetail});
      if (userDetail) {
        LoginApi(JSON.parse(userDetail), responseFunction);

      } else {
        props.navigation.navigate('LoginScreen', {
          token: fcmToken ? fcmToken : 'No token ',
        });
      }
      // LoginApi(
      //   {companyCode: 'KWIK001', userName: 'admin', password: '111'},
      //   loginResponseHandler,
      // );

      return () => {
        timeout.clear();
      };
    }, 3000);
  },[]);

  const loginResponseHandler = async res => {
    if (!res.success) {
      alert(`Network Error: check your internet connection and try again`);
    } else {
      let data = res.responseBody.children;
      if (data) {
        let USER_DETAIL = {};
        data.forEach(element => {
          USER_DETAIL[element.name] = element.value;
        });
        if (USER_DETAIL.isError === 'True') {
          alert(USER_DETAIL.Result);
        } else {
          props.dispatch({type: 'USER_DETAIL', USER_DETAIL});
          props.navigation.navigate('HomeScreen');
        }
      }
    }
  };

  const getCustomerResponseHandler = async resp => {
    try {
      const data =
        resp.responseBody.children[0].children[0].children[0].children;
      let obj = {};
      data.forEach(element => {
        obj[element.name] = element.value;
      });
      props.dispatch({type: 'USER_DETAIL', USER_DETAIL: obj});
      props.navigation.navigate('HomeScreen');
    } catch (e) {
      console.log(JSON.stringify(e));
    }
  };

  return (
    <ImageBackground style={styles.container} source={Images.mainBackground}>
      <Image source={Images.SPLASH_LOGO} style={styles.LogoImage} />
    </ImageBackground>
  );
};
const mapStateToProps = state => {
  return {USER_DETAIL: state.USER_DETAIL};
};
export default connect(mapStateToProps)(SplashScreen);
