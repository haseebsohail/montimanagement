import {StyleSheet} from 'react-native';
import {Colors} from '../../Theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  LogoImage: {width: 200, height: 100},
});

export default styles;
