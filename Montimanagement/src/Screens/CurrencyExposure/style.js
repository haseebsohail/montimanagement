import styled from 'styled-components/native';

import {Colors} from '../../Theme';
import colors from '../../Theme/Colors';

export const Wrapper = styled.View`
  background-color: ${Colors.WHITE};
  flex: 1;
`;

export const Row = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  margin-horizontal: 5%;
  background-color: #e6e6e6;
  border-radius: 10px;
  padding-bottom: 20px;
`;

export const DateSeletorContainer = styled.View`
  flex-direction: row;
  justify-content: center;
  margin-vertical: 20px;
`;

export const ModalWrapper = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1;
  background-color: rgba(0, 0, 0, 0.5);
`;

export const ModalLabelWrapper = styled.View`
  align-items: center;
  flex: 1;
`;

export const ModalLabelText = styled.Text`
  color: ${Colors.MAIN_COLOR};
  font-size: 16px;
  font-weight: 600;
`;

export const ModalCloseIconWrapper = styled.TouchableOpacity`
  margin: 10px;
  flex-direction: row;
`;

export const ModalMainContainer = styled.View`
  width: 90%;
  border-radius: 10px;
  padding-bottom: 20px;
  border-color: rgba(0, 0, 0, 0.2);
  background-color: white;
  border-width: 1px;
  border-radius: 10px;
  min-height: 40%;
  max-height: 90%;
`;

export const ButtonWrapper = styled.TouchableOpacity`
  width: 90%;
  margin-top: 5px;
  border-radius: 5px;
  background-color: ${props =>
    props.isSelected ? colors.MAIN_COLOR : 'white'};
  flex-direction: column;
  justify-content: center;
  align-items: center;
  align-self: center;
  padding: 10px;

  shadow-color: #000;
  shadow-opacity: 0.3;
  shadow-radius: 5px;
  elevation: 14;
`;

export const BalanceContainerLabel = styled.View`
  width: 100%;
  height: 50px;
  align-items: center;
  justify-content: center;
`;

export const BalancesButtonWrapper = styled.View`
  width: 96%;
  margin-left: 2%;
  margin-top: 2px;
  background-color: ${props =>
    props.isSelected ? Colors.MAIN_COLOR : 'white'};
  flex-direction: row;
  justify-content: space-between;
  padding: 10px;
  shadow-color: #000;
  shadow-opacity: 0.3;
  shadow-radius: 5px;
  elevation: 14;
`;

export const CurrecyWrapper = styled.View`
  width: 96%;
  margin-left: 2%;
  margin-top: 2px;
  background-color: ${props => (props.isSelected ? Colors.MAIN_COLOR : 'grey')};
  flex-direction: row;
  justify-content: space-between;
  padding: 10px;
  shadow-color: #000;
  shadow-opacity: 0.3;
  shadow-radius: 5px;
  elevation: 14;
`;

export const TotalBalanceWrap = styled.View`
  width: 96%;
  margin-left: 2%;
  margin-top: 2px;
  flex-direction: row;
  justify-content: space-between;
  padding: 10px;
  shadow-color: #000;
  shadow-opacity: 0.3;
  shadow-radius: 5px;
  elevation: 14;
`;

export const TotalBalanceText = styled.Text`
  font-size: 16px;
  font-weight: 600;
  text-align: right;
  width: 100%;
`;

export const BookedBalancesButtonWrapper = styled.View`
  width: 30%;
  margin-left: 3%;
  margin-top: 10px;
  border-radius: 5px;
  background-color: ${Colors.themeGrey};
  flex-direction: column;
  justify-content: center;
  align-items: center;
  align-self: center;
  padding: 10px;

  shadow-color: #000;
  shadow-opacity: 0.3;
  shadow-radius: 5px;
  elevation: 14;
`;

export const BalanceButtonText = styled.Text`
  color: ${props => (props.isSelected ? Colors.white : Colors.themeGrey)};
  font-weight: 600;
`;

// export const BalanceText = styled.Text`
//   color: ${props => (props.isSelected ? Colors.white : Colors.themeGrey)};
//   font-weight: 600;
// `;

export const BalanceCorrespondent = styled.Text`
  color: ${props => (props.isSelected ? Colors.white : Colors.themeGrey)};
  font-weight: 600;
  width: 70%;
`;

export const ButtonText = styled.Text`
  color: ${props => (props.isSelected ? Colors.white : Colors.themeGrey)};
  font-weight: 600;
`;

export const ConfirmationButtonWrapper = styled.TouchableOpacity`
  width: 50%;
  margin-top: 10px;
  border-radius: 5px;
  background-color: ${Colors.MAIN_COLOR};
  justify-content: center;
  align-items: center;
  padding: 10px;

  shadow-color: ${Colors.MAIN_COLOR};
  shadow-opacity: 0.3;
  shadow-radius: 5px;
  elevation: 14;
`;

export const TableCol = styled.View`
  justify-content: center;
  padding-horizontal: 10px;
  padding-vertical: 5px;
  border-width: 0.5px;
  border-color: ${Colors.SepratorLine};
  ${props => props.width && `width: ${props.width}px`}
  ${props =>
    props.header
      ? `
        background-color: ${Colors.MAIN_COLOR};
      `
      : `
      `}
`;
export const TableRow = styled.View`
  flex-direction: row;
  ${props =>
    props.header &&
    `
      background-color: ${Colors.MAIN_COLOR};
      shadow-color: #000;
      shadow-opacity: 0.3;
      shadow-radius: 5px;
      elevation: 14;
    `}

  ${props =>
    props.index % 2 !== 0 &&
    `
        background-color: #f7ffed;
      `}
`;

export const TableHeaderText = styled.Text`
  color: ${Colors.white};
  font-weight: bold;
  align-self: center;
`;

export const DetailContainer = styled.View`
  flex: 1;
  margin-top: 10px;
`;
export const DetailWrapper = styled.View`
  width: 100%;
  padding: 10px;
  border-width: 1px;
  border-color: ${Colors.SepratorLine};
  background-color: #ffffff55;
`;

export const TitleContainer = styled.Text`
  font-size: 18px;
  font-weight: bold;
  color: ${Colors.MAIN_COLOR};
`;

export const AmountContainer = styled.View`
  flex-direction: row;
`;

export const LabelText = styled.Text`
  ${props =>
    props.amount
      ? `
        font-size: 16px;
        `
      : `
        font-size: 12px;    
      `}
  color: #737373;
  font-weight: bold;
  flex: 1;
`;

export const AmountText = styled.Text`
  ${props =>
    props.amount
      ? props.amount > 0
        ? `color: ${Colors.MAIN_COLOR}; 
        font-size: 16px;`
        : `color: ${Colors.warningColor}; 
        font-size: 16px;`
      : `
      color: #737373;
      font-size: 12px;
      `}

  font-weight: bold;
`;

export const DetailView = styled.View`
  flex-direction: row;
`;
export const StatusView = styled.View`
  justify-content: center;
  align-items: center;
`;
export const NameDetail = styled.View`
  margin-left: 15px;
  flex: 1;
`;
export const NameText = styled.Text`
  font-size: 16px;
  color: ${Colors.MAIN_COLOR};
`;
export const VrNoText = styled.Text`
  font-size: 8px;
`;
export const PayModeText = styled.Text`
  font-size: 8px;
  color: ${Colors.BLACK};
`;
export const VoucherNoText = styled.Text`
  font-size: 16px;
  color: ${Colors.MAIN_COLOR};
`;
export const AmountDetail = styled.View`
  margin-left: 15px;
  width: 30%;
`;
export const Details = styled.View``;
export const LCView = styled.View`
  flex-direction: row;
`;
export const AmountLabel = styled.Text`
  font-size: 8px;
  color: ${Colors.GREY1};
`;
export const DetailText = styled.Text`
  font-size: 9px;
`;
export const StatusText = styled.Text`
  font-size: 9px;
`;
export const BalanceText = styled.Text`
  font-size: 16px;
  color: ${props => (props.amount < 0 ? Colors.Red : Colors.MAIN_COLOR)};
`;
export const CCYText = styled.Text`
  font-size: 8px;
  margin-top: 5px;
  color: ${Colors.MAIN_COLOR};
`;

export const FCView = styled.View`
  flex-direction: row;
`;
export const FCText = styled.Text`
  font-size: 8px;
  color: ${Colors.GREY1};
`;
export const FCAmountText = styled.Text`
  font-size: 16px;
  color: ${Colors.MAIN_COLOR};
`;
export const VrDateText = styled.Text`
  font-size: 8px;
`;
export const CurrencyHeading = styled.Text`
  font-size: 12px;
  color: #737373;
  font-weight: bold;
`;
// Button Wrapper

export const MainContainer = styled.View`
  flex: 1;
  align-items: center;
`;

export const SelectionItemContainer = styled.View`
  flex-direction: column;
  width: 100%;
  align-items: center;
  margin-top: 10px;
`;

export const SelectionItemLabel = styled.Text`
  width: 100%;
  font-size:20px;
  margin-top: 10px;
  margin-bottom: 10px;
  margin-left: 20px;
  text-align:left;
  color:#000;
  font-weight:600;
`;

export const BranchSelectorWrapper = styled.TouchableOpacity`
  border-radius: 5px;
  background-color: ${props =>
    props.isSelected ? colors.MAIN_COLOR : 'white'};
  flex-direction: column;
  justify-content: center;
  align-items: center;
  align-self: center;
  padding: 15px;
width: 80%;
  shadow-color: #000;
  shadow-opacity: 0.1;
  shadow-radius: 5px;
  elevation: 14;
`;

export const LoadDataButton = styled.TouchableOpacity`
  border-radius: 5px;
  background-color: ${colors.MAIN_COLOR};

  justify-content: center;
  align-items: center;
  padding: 10px;

  shadow-color: #000;
  shadow-opacity: 0.1;
  shadow-radius: 5px;
  elevation: 14;
`;
