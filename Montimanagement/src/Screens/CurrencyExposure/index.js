import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Modal,
  ImageBackground,
  ScrollView,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
// import DateTimePicker from '@react-native-community/datetimepicker';

import {
  CMB_AgentBranchesAPI,
  GetCurrencyExposureAPI,
} from '../../Store/apiCalls';
import {MainNavigationHeader, DateTimePicker} from '../../Components';
import {Images} from '../../Theme/';
import {CloseSVG} from '../../Theme/SVG';
import {dateFormat, formatAmount} from '../../Theme/utils';

import {
  Wrapper,
  MainContainer,
  SelectionItemContainer,
  SelectionItemLabel,
  ModalWrapper,
  ModalLabelWrapper,
  ModalLabelText,
  ButtonWrapper,
  ButtonText,
  ModalMainContainer,
  ModalCloseIconWrapper,
  BalanceButtonText,
  ConfirmationButtonWrapper,
  DetailWrapper,
  TitleContainer,
  AmountContainer,
  LabelText,
  AmountText,
  BranchSelectorWrapper,
} from './style';
import {Colors} from '../../Theme';

const CurrencyStock = props => {
  const [showBranchesModal, setShowBranchesModal] = useState(false);
  const [branchesList, setBranchesList] = useState([]);
  const [selectedBranches, setSelectedBranches] = useState(null);
  const [exposureData, setExposureData] = useState(null);
  const [selectedDate, setSelectedDate] = useState(new Date());

  useEffect(() => {
    if (props.USER_DETAIL) {
      const obj = {
        id: props.USER_DETAIL.BranchID,
        name: props.USER_DETAIL.BranchName,
      };
      setSelectedBranches(obj);
      let agent_GUID = props.USER_DETAIL.CC_GUID;
      CMB_AgentBranchesAPI(agent_GUID, CMB_AgentBranchesAPIHandler);
    }
  }, [props.USER_DETAIL]);

  // Onfocus event handler
  React.useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      let agent_GUID = props.USER_DETAIL.CC_GUID;
      CMB_AgentBranchesAPI(agent_GUID, CMB_AgentBranchesAPIHandler);
    });
    return unsubscribe;
  }, [props.navigation, props.USER_DETAIL]);

  const CMB_AgentBranchesAPIHandler = res => {
    let obj = [{id: 0, name: 'None'}];
    res.responseBody.children[1].children[0].children.forEach(element => {
      obj.push({
        name: element.children[0].value,
        id: element.children[1].value,
      });
    });
    setBranchesList(obj);
  };

  // submit Button
  const submit = () => {
    let body = {
      CCID: selectedBranches.id,
      strEnd: dateFormat(selectedDate),
    };
    GetCurrencyExposureAPI(body, GetCurrencyExposureAPIHandler);
  };

  const GetCurrencyExposureAPIHandler = res => {
    let datalist = res.responseBody.children[1].children;

    if (datalist.length > 0) {
      const data = res.responseBody.children[1].children[0].children;

      let objList = [];
      data.forEach(item => {
        let row = {};
        item.children.forEach(element => {
          row[element.name] = element.value;
        });
        objList.push(row);
      });

      setExposureData(objList);
    } else {
      Alert.alert('Álert', 'No data found');
    }
  };

  return (
    <Wrapper>
      <MainNavigationHeader HeaderText="Currency Exposure" />
      <ImageBackground
        source={Images.mainBackground}
        style={{width: '100%', flex: 1}}>
        {/* Branches Modal */}
        <Modal
          animationType="fade"
          transparent={true}
          visible={showBranchesModal}
          onRequestClose={() => {
            setShowBranchesModal(!showBranchesModal);
          }}>
          <ModalWrapper>
            <ModalMainContainer>
              <ModalCloseIconWrapper
                onPress={() => setShowBranchesModal(false)}>
                <ModalLabelWrapper>
                  <ModalLabelText>Select a Branch</ModalLabelText>
                </ModalLabelWrapper>
                <CloseSVG width={25} height={25} />
              </ModalCloseIconWrapper>
              <ScrollView>
                {branchesList &&
                  branchesList.map((element, index) => {
                    return (
                      <ButtonWrapper
                        key={`branch${index}`}
                        isSelected={
                          selectedBranches
                            ? selectedBranches.id === element.id
                            : false
                        }
                        onPress={() => {
                          setSelectedBranches(element);
                          setShowBranchesModal(false);
                        }}>
                        <ButtonText
                          isSelected={
                            selectedBranches
                              ? selectedBranches.id === element.id
                              : false
                          }>
                          {element.name}
                        </ButtonText>
                      </ButtonWrapper>
                    );
                  })}
              </ScrollView>
            </ModalMainContainer>
          </ModalWrapper>
        </Modal>

        <ScrollView>
          {/* Selection Container */}
          <MainContainer>
            {/* Data Picker */}
            <SelectionItemContainer>
              <SelectionItemLabel>Date</SelectionItemLabel>
              <DateTimePicker
                mode={'date'}
                value={selectedDate}
                onChange={(e, d) => {
                  setSelectedDate(d);
                }}
              />
            </SelectionItemContainer>

            {/* Branch */}
            <SelectionItemContainer>
              <SelectionItemLabel>Branch</SelectionItemLabel>
              <BranchSelectorWrapper onPress={() => setShowBranchesModal(true)}>
                <Text>
                  {selectedBranches ? selectedBranches.name : 'Select Branch'}
                </Text>
              </BranchSelectorWrapper>
            </SelectionItemContainer>

            <ConfirmationButtonWrapper onPress={() => submit()}>
              <BalanceButtonText isSelected={true}>Submit</BalanceButtonText>
            </ConfirmationButtonWrapper>
            {exposureData &&
              exposureData.map((element, index) => {
                return (
                  <DetailWrapper>
                    <TitleContainer>{element.CCNAME}</TitleContainer>

                    <Text
                      style={{
                        fontSize: 14,
                        color: Colors.themeGreyNew,
                        marginBottom: 10,
                        fontWeight: 'bold',
                      }}>
                      Currency: {element.CCY}
                    </Text>
                    <View>
                      <AmountContainer>
                        <LabelText>CASH IN HAND</LabelText>
                        <AmountText>
                          {formatAmount(element.CASH_IN_HAND)}
                        </AmountText>
                      </AmountContainer>

                      <AmountContainer>
                        <LabelText>CORRESPONDENT:</LabelText>
                        <AmountText>
                          {formatAmount(element.CORRESPONDENT)}
                        </AmountText>
                      </AmountContainer>
                      <AmountContainer>
                        <LabelText>BOOKING BALANCE:</LabelText>
                        <AmountText>
                          {formatAmount(element.BOOKING_BAL)}
                        </AmountText>
                      </AmountContainer>
                    </View>
                    <View>
                      <AmountContainer>
                        <LabelText>BANK BALANCE:</LabelText>
                        <AmountText>
                          {formatAmount(element.BANK_INSIDE_UAE)}
                        </AmountText>
                      </AmountContainer>
                      <AmountContainer>
                        <LabelText amount={element.NET_BALANCE}>
                          NET BALANCE:
                        </LabelText>
                        <AmountText amount={element.NET_BALANCE}>
                          {formatAmount(element.NET_BALANCE)}
                        </AmountText>
                      </AmountContainer>
                      <AmountContainer>
                        <LabelText amount={element.NET_BALANCE}>
                          EQUAL AMOUNT:
                        </LabelText>
                        <AmountText amount={element.NET_BALANCE}>
                          {formatAmount(element.REVALUATION_BALANCE)}
                        </AmountText>
                      </AmountContainer>
                    </View>
                  </DetailWrapper>
                );
              })}
          </MainContainer>
        </ScrollView>
      </ImageBackground>
    </Wrapper>
  );
};

const mapStateToProps = state => {
  return {USER_DETAIL: state.USER_DETAIL};
};

export default connect(mapStateToProps)(CurrencyStock);
