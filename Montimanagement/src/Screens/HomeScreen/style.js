import styled from 'styled-components/native';
import {Colors, Constants} from '../../Theme';

export const Wrapper = styled.View`
  background-color: ${Colors.WHITE};
  flex: 1;
`;

export const StyledText = styled.Text`
  color: palevioletred;
`;

export const Row = styled.View`
  flex-direction: row;
  margin-vertical: 20px;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  width:100%;
`;

export const ButtonContainer = styled.View`
  width: 44%;
  margin-left: 4%;
`;

export const ButtonWrapper = styled.TouchableOpacity`
  width:100px;
  height: 100px;
  margin-left: 4%;
  margin-top: 10px;
  border-radius:100px;
  background-color: ${Colors.MAIN_COLOR};
  flex-direction: column;
  justify-content: center;
  align-items: center;

  shadow-color: #000;
  shadow-opacity: 0.41;
  shadow-radius: 10px;
  elevation: 14;
`;

export const ButtonText = styled.Text`
  color: ${Colors.MAIN_COLOR};
  margin-top: 10px;
  font-size: 12px;
  font-weight: bold;
  text-align: center;
  flex-wrap: wrap;
  flex-direction: row;


`;

export const TransNumberText = styled.Text`
  color: ${Colors.MAIN_COLOR};
  margin-left: 10px;
  font-size: 12px;
  font-weight: bold;
`;

export const ButtonImage = styled.Image`
  width: 80px;
  height: 80px;
`;

export const QrContainer = styled.View`
  align-items: center;
`;
export const CSWrapper = styled.View`
  align-items: center;
  margin-vertical: 20px;
`;
export const CSCircle = styled.TouchableOpacity`
  border-color: ${Colors.MAIN_COLOR};
  align-items: center;
  justify-content: center;
  width: ${Constants.windowWidth - 100}px;
  height: ${Constants.windowWidth - 100}px;
  border-width: 3px;
  border-radius: ${(Constants.windowWidth - 100) / 2}px;
`;
export const BalanceText = styled.Text`
  font-size: 40px;
  opacity: 0.7;
  margin-top: 10px;
`;
export const AedText = styled.Text`
  font-size: 20px;
  opacity: 0.7;
  margin-top: 10px;
`;
export const CSText = styled.Text`
  font-size: 12px;
  opacity: 0.5;
  margin-top: 10px;
`;
export const WelcomeView = styled.View`
  align-items: center;
  margin-vertical: 20px;
`;
export const WelcomeText = styled.Text`
  font-size: 20px;
  font-weight: bold;
  margin-top: 10px;
`;
