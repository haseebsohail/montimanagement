import React, {useEffect} from 'react';
import {BackHandler, ImageBackground, Alert, ScrollView} from 'react-native';
import {connect} from 'react-redux';
import {Colors} from '../../Theme';
import {MainNavigationHeader} from '../../Components';
import {GetCurrencyStockAPI, fnGetRoRiTotal} from '../../Store/apiCalls';
import {dateFormat, formatAmount} from '../../Theme/utils';

import {
  LogoutSVG,
  UserSVG,
  DollorBagSVG,
  FinancialAccountSVG,
  FinancialProfitSVG,
  InSVG,
  OutSVG,
} from '../../Theme/SVG';

import {
  Wrapper,
  Row,
  ButtonWrapper,
  ButtonText,
  CSWrapper,
  CSCircle,
  BalanceText,
  CSText,
  AedText,
  WelcomeView,
  WelcomeText,
  TransNumberText,
} from './style';
import {Images} from '../../Theme/';
import {View} from 'native-base';
import { AsyncStorage } from '../../Theme/Libraries';

const HomeScreen = props => {
  const [balanceAed, setBalanceAed] = React.useState();
  const [roRiDetail, setRoRiDetail] = React.useState();
  const buttonsList = [
    {
      buttonTitle: 'CURRESPONDENT BALANCE',
      buttonOnPress: () => props.navigation.navigate('CorrespondentBalance'),
      icon: <UserSVG fill={Colors.white} width={30} height={30} />,
    },
    {
      buttonTitle: 'CURRENCY STOCK',
      buttonOnPress: () => props.navigation.navigate('CurrencyStock'),
      icon: <DollorBagSVG fill={Colors.white} width={30} height={30} />,
    },
    {
      buttonTitle: 'CURRENCY EXPOSURE',
      buttonOnPress: () => props.navigation.navigate('CurrencyExposure'),
      icon: (
        <FinancialProfitSVG fill={Colors.white} width={30} height={30} />
      ),
    },
    {
      buttonTitle: 'FCY LEDGER',
      buttonOnPress: () => props.navigation.navigate('FCYLedger'),
      icon: (
        <FinancialAccountSVG fill={Colors.white} width={30} height={30} />
      ),
    },
    {
      buttonTitle: 'REMITTANCE INWARD',
      buttonOnPress: () => props.navigation.navigate('RemittanceInward'),
      icon: <InSVG fill={Colors.white} width={30} height={30} />,
      Amount: roRiDetail && roRiDetail[3].RITotal,
    },
    {
      buttonTitle: 'REMITTANCE OUTWARD',
      buttonOnPress: () => props.navigation.navigate('RemittanceOutward'),
      icon: <OutSVG fill={Colors.white} width={30} height={30} />,
      Amount: roRiDetail && roRiDetail[2].RoTotal,
    },
  ];

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Hold on!', 'Are you sure you want exit the app?', [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'YES', onPress: () => BackHandler.exitApp()},
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    if (props.USER_DETAIL) {
      let CurrencyBody = {
        CCID: props.USER_DETAIL.BranchID,
        strEnd: dateFormat(new Date()),
      };
      GetCurrencyStockAPI(CurrencyBody, GetCurrencyStockAPIHandler);

      let fnGetRoRiTotalBody = {
        strFrom: dateFormat(new Date()),
        strTo: dateFormat(new Date()),
      };
      fnGetRoRiTotal(fnGetRoRiTotalBody, fnGetRoRiTotalHandler);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fnGetRoRiTotalHandler = res => {
    let data = res.responseBody.children;
    let objList = [];
    data.forEach(item => {
      objList.push({[item.name]: item.value});
    });
    if (objList[0].isError === 'False') {
      setRoRiDetail(objList);
    }
  };

  const GetCurrencyStockAPIHandler = res => {
    let datalist = res.responseBody.children[1].children;

    if (datalist.length > 0) {
      const data = res.responseBody.children[1].children[0].children;

      let objList = [];
      data.forEach(item => {
        let row = {};
        item.children.forEach(element => {
          row[element.name] = element.value;
        });
        objList.push(row);
      });

      // Seprate Currency Data
      let cl = [];
      let newObj = {};
      objList.forEach(element => {
        if (newObj[element.CCName]) {
          newObj[element.CCName].push(element);
        } else {
          cl.push(element.CCName);
          newObj[element.CCName] = [element];
        }
      });

      let currencyBalance = {};
      cl.forEach(item => {
        let balance = 0;
        newObj[item].forEach(element => {
          balance = balance + (element.DR - element.CR);
        });
        currencyBalance[item] = balance;
      });

      let branchAedBalance = 0;
      newObj[props.USER_DETAIL.BranchName].forEach(element => {
        if (element.CurrencyShortName === 'AED') {
          branchAedBalance = element.BALANCE;
        }
      });
      setBalanceAed(branchAedBalance);
    } else {
      Alert.alert('Álert', 'No data found');
    }
  };

  return (
    <Wrapper>
      <ImageBackground
        source={Images.homeBackground}
        style={{width: '100%', flex: 1}}>
        <MainNavigationHeader
          HeaderText={props.USER_DETAIL.BranchName}
          RightIcon={<LogoutSVG height={35} width={25} fill="white" />}
          RightOnPress={() => {
            AsyncStorage.removeItem('@credientials')
            props.navigation.navigate('LoginScreen')}}
        />

        <ScrollView>
          <WelcomeView>
            <UserSVG height={35} width={35} fill={Colors.MAIN_COLOR} />
            <WelcomeText>
              Welcome {props.USER_DETAIL ? props.USER_DETAIL.UserName : 'User'}
            </WelcomeText>
          </WelcomeView>

          <CSWrapper>
            <CSCircle
              onPress={() => props.navigation.navigate('CurrencyStock')}>
              <BalanceText>{formatAmount(balanceAed)}</BalanceText>
              <AedText>AED</AedText>
              <CSText>Currency Stock</CSText>
            </CSCircle>
          </CSWrapper>
          <Row>
            {buttonsList.map((element, index) => {
              return (
                <View style={{
                  flexWrap:'wrap',
                  margin:5,
                  width:150,
                  height:160,
                  flexDirection:'row',
                  justifyContent:'center',
                  alignContent:'center'                
                }}>
                <ButtonWrapper
                  key={`Button${index}`}
                  onPress={element.buttonOnPress}>
                  <View style={{flexDirection: 'row'}}>
                    {element.icon}
                    {element.Amount && (
                      <TransNumberText>{element.Amount}</TransNumberText>
                    )}
                  </View>

                </ButtonWrapper>
                  <ButtonText>{element.buttonTitle}</ButtonText>
                  </View>
              );
            })}
          </Row>
        </ScrollView>
      </ImageBackground>
    </Wrapper>
  );
};

const mapStateToProps = state => {
  return {USER_DETAIL: state.USER_DETAIL};
};

export default connect(mapStateToProps)(HomeScreen);
