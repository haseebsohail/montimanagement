import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Modal,
  ScrollView,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
// import DateTimePicker from '@react-native-community/datetimepicker';

import {CMB_AgentBranchesAPI, GetCurrencyStockAPI} from '../../Store/apiCalls';
import {MainNavigationHeader, DateTimePicker} from '../../Components';
import {Images} from '../../Theme';
import {CloseSVG} from '../../Theme/SVG';
import {dateFormat, formatAmount} from '../../Theme/utils';

import {
  Wrapper,
  Row,
  MainContainer,
  ModalWrapper,
  ModalLabelWrapper,
  ModalLabelText,
  ButtonWrapper,
  ButtonText,
  BalanceCorrespondent,
  ModalMainContainer,
  ModalCloseIconWrapper,
  BalancesButtonWrapper,
  BalanceButtonText,
  BalanceText,
  TotalBalanceText,
  TotalBalanceWrap,
  ConfirmationButtonWrapper,
  CurrecyWrapper,
} from './style';

const CurrencyStock = props => {
  const [showBranchesModal, setShowBranchesModal] = useState(false);
  const [branchesList, setBranchesList] = useState([]);
  const [currencyCorrespondentList, setCurrencyCorrespondentList] =
    useState(null);
  const [currencyCorrespondentDetail, setCurrencyCorrespondentDetail] =
    useState(null);
  const [currencyBalanceDetail, setCurrencyBalanceDetail] = useState(null);
  const [selectedBranches, setSelectedBranches] = useState(null);

  const [selectedDate, setSelectedDate] = useState(new Date());

  useEffect(() => {
    if (props.USER_DETAIL) {
      const obj = {
        id: props.USER_DETAIL.BranchID,
        name: props.USER_DETAIL.BranchName,
      };
      setSelectedBranches(obj);
      let agent_GUID = props.USER_DETAIL.CC_GUID;
      CMB_AgentBranchesAPI(agent_GUID, CMB_AgentBranchesAPIHandler);
    }
  }, [props.USER_DETAIL]);

  // Onfocus event handler
  React.useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      let agent_GUID = props.USER_DETAIL.CC_GUID;
      CMB_AgentBranchesAPI(agent_GUID, CMB_AgentBranchesAPIHandler);
    });
    return unsubscribe;
  }, [props.navigation, props.USER_DETAIL]);

  const CMB_AgentBranchesAPIHandler = res => {
    let obj = [{id: 0, name: 'None'}];
    res.responseBody.children[1].children[0].children.forEach(element => {
      obj.push({
        name: element.children[0].value,
        id: element.children[1].value,
      });
    });
    setBranchesList(obj);
  };

  // submit Button
  const submit = () => {
    setCurrencyCorrespondentList();
    // let body = {
    //   CCID: 0,
    //   strEnd: dateFormat(selectedDate),
    // };
    // GetCurrencyStockAPI(body, GetCurrencyStockAPIHandler);

    let body = {
      CCID: selectedBranches.id,
      strEnd: dateFormat(selectedDate),
    };
    GetCurrencyStockAPI(body, GetCurrencyStockAPIHandler);
  };

  const GetCurrencyStockAPIHandler = res => {
    let datalist = res.responseBody.children[1].children;

    if (datalist.length > 0) {
      const data = res.responseBody.children[1].children[0].children;

      let objList = [];
      data.forEach(item => {
        let row = {};
        item.children.forEach(element => {
          row[element.name] = element.value;
        });
        objList.push(row);
      });

      // Seprate Currency Data
      let cl = [];
      let newObj = {};
      objList.forEach(element => {
        if (newObj[element.CCName]) {
          newObj[element.CCName].push(element);
        } else {
          cl.push(element.CCName);
          newObj[element.CCName] = [element];
        }
      });

      let currencyBalance = {};
      cl.forEach(item => {
        let balance = 0;
        newObj[item].forEach(element => {
          balance = balance + (element.DR - element.CR);
        });
        currencyBalance[item] = balance;
      });

      setCurrencyBalanceDetail(currencyBalance);
      setCurrencyCorrespondentList(cl);
      setCurrencyCorrespondentDetail(newObj);
    } else {
      Alert.alert('Álert', 'No data found');
    }
  };

  return (
    <Wrapper>
      <MainNavigationHeader HeaderText="Currency Stock" />
      <ImageBackground
        source={Images.mainBackground}
        style={{width: '100%', flex: 1}}>
        <ScrollView>
          {/* Branches Modal */}
          <Modal
            animationType="fade"
            transparent={true}
            visible={showBranchesModal}
            onRequestClose={() => {
              setShowBranchesModal(!showBranchesModal);
            }}>
            <ModalWrapper>
              <ModalMainContainer>
                <ModalCloseIconWrapper
                  onPress={() => setShowBranchesModal(false)}>
                  <ModalLabelWrapper>
                    <ModalLabelText>Select a Branch</ModalLabelText>
                  </ModalLabelWrapper>
                  <CloseSVG width={25} height={25} />
                </ModalCloseIconWrapper>
                <ScrollView>
                  {branchesList &&
                    branchesList.map((element, index) => {
                      return (
                        <ButtonWrapper
                          key={`branch${index}`}
                          isSelected={
                            selectedBranches
                              ? selectedBranches.id === element.id
                              : false
                          }
                          onPress={() => {
                            setSelectedBranches(element);
                            setShowBranchesModal(false);
                          }}>
                          <ButtonText
                            isSelected={
                              selectedBranches
                                ? selectedBranches.id === element.id
                                : false
                            }>
                            {element.name}
                          </ButtonText>
                        </ButtonWrapper>
                      );
                    })}
                </ScrollView>
              </ModalMainContainer>
            </ModalWrapper>
          </Modal>

          <MainContainer>
            {/* Branches Button */}
            <ButtonWrapper onPress={() => setShowBranchesModal(true)}>
              <ButtonText>
                {selectedBranches ? selectedBranches.name : 'Select Branch'}
              </ButtonText>
            </ButtonWrapper>

            {/* Date Picker */}
            <View style={{margin: 10,width:'80%'}}>
              <DateTimePicker
                mode={'date'}
                value={selectedDate}
                onChange={(e, d) => {
                  setSelectedDate(d);
                }}
              />
            </View>

            {currencyCorrespondentList && (
              <Row>
                <BalancesButtonWrapper isSelected key={'BalanceHeader'}>
                  <BalanceCorrespondent isSelected>
                    Currency Code & Name
                  </BalanceCorrespondent>
                  <BalanceText isSelected> Amount</BalanceText>
                </BalancesButtonWrapper>
                {currencyCorrespondentList.map((item, ind) => {
                  return (
                    <View style={{width: '100%'}} key={`main${ind}`}>
                      <CurrecyWrapper key={`Currency${ind}`}>
                        <Text>{item}</Text>
                      </CurrecyWrapper>
                      {currencyCorrespondentDetail &&
                        currencyCorrespondentDetail[item] &&
                        currencyCorrespondentDetail[item].map(
                          (element, index) => {
                            return (
                              <BalancesButtonWrapper key={`Balance${index}`}>
                                <BalanceCorrespondent>
                                  {`${element.CurrencyShortName}     ${element.CurrencyName}`}
                                </BalanceCorrespondent>
                                <BalanceText>
                                  {formatAmount(element.DR - element.CR)}
                                </BalanceText>
                              </BalancesButtonWrapper>
                            );
                          },
                        )}
                      <TotalBalanceWrap key={`TotalBalance${ind}`}>
                        <TotalBalanceText>
                          {`Total Balance   ${
                            currencyBalanceDetail && currencyBalanceDetail[item]
                          }`}
                        </TotalBalanceText>
                      </TotalBalanceWrap>
                    </View>
                  );
                })}
              </Row>
            )}
            <ConfirmationButtonWrapper onPress={() => submit()}>
              <BalanceButtonText isSelected={true}>Submit</BalanceButtonText>
            </ConfirmationButtonWrapper>
          </MainContainer>
        </ScrollView>
      </ImageBackground>
    </Wrapper>
  );
};

const mapStateToProps = state => {
  return {USER_DETAIL: state.USER_DETAIL};
};

export default connect(mapStateToProps)(CurrencyStock);
