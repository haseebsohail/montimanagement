import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  Modal,
  ImageBackground,
  ScrollView,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';

import {CMB_AgentBranchesAPI, GetRIListing} from '../../Store/apiCalls';
import {
  MainNavigationHeader,
  DateTimePicker as DatePicker,
} from '../../Components';
import {Images} from '../../Theme';
import {
  BackSVG,
  CloseSVG,
  ClearedSVG,
  CancelSVG,
  PendingSVG,
} from '../../Theme/SVG';
import {dateFormat, formatAmount} from '../../Theme/utils';

import {
  Wrapper,
  MainContainer,
  SelectionItemContainer,
  SelectionItemLabel,
  ModalWrapper,
  ModalLabelWrapper,
  ModalLabelText,
  ButtonWrapper,
  ButtonText,
  ModalMainContainer,
  ModalCloseIconWrapper,
  BalanceButtonText,
  DetailText,
  ConfirmationButtonWrapper,
  DetailContainer,
  DetailWrapper,
  DetailView,
  StatusView,
  NameDetail,
  NameText,
  VrNoText,
  PayModeText,
  AmountDetail,
  Details,
  LCView,
  LCText,
  AmountText,
  CCYText,
  FCView,
  FCText,
  FCAmountText,
  VrDateText,
  BranchSelectorWrapper,
} from './style';

const RemittanceInward = props => {
  const [showBranchesModal, setShowBranchesModal] = useState(false);
  const [branchesList, setBranchesList] = useState([]);
  const [selectedBranches, setSelectedBranches] = useState(null);

  const [remmitanceData, setRemmitanceData] = useState(null);
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());

  useEffect(() => {
    if (props.USER_DETAIL) {
      const obj = {
        id: props.USER_DETAIL.BranchID,
        name: props.USER_DETAIL.BranchName,
      };
      setSelectedBranches(obj);
    }
  }, [props.USER_DETAIL]);
  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      let agent_GUID = props.USER_DETAIL.CC_GUID;
      CMB_AgentBranchesAPI(agent_GUID, CMB_AgentBranchesAPIHandler);
    });
    return unsubscribe;
  }, [props.navigation, props.USER_DETAIL]);

  const CMB_AgentBranchesAPIHandler = res => {
    let obj = [{id: 0, name: 'None'}];
    res.responseBody.children[1].children[0].children.forEach(element => {
      obj.push({
        name: element.children[0].value,
        id: element.children[1].value,
      });
    });
    setBranchesList(obj);
  };

  // submit Button
  const submit = () => {
    let body = {
      CCID: selectedBranches.id,
      strTo: dateFormat(endDate),
      strFrom: dateFormat(startDate),
    };
    GetRIListing(body, GetRIListingHandler);
  };

  const GetRIListingHandler = res => {
    let datalist = res.responseBody.children[1].children;

    if (datalist.length > 0) {
      const data = res.responseBody.children[1].children[0].children;

      let objList = [];
      data.forEach(item => {
        let row = {};
        item.children.forEach(element => {
          row[element.name] = element.value;
        });
        objList.push(row);
      });
      setRemmitanceData(objList);
    } else {
      Alert.alert('Álert', 'No data found');
    }
  };

  return (
    <Wrapper>
      <ImageBackground
        source={Images.mainBackground}
        style={{width: '100%', flex: 1}}>
        <MainNavigationHeader HeaderText="Remittance Inward" />

        {/* Branches Modal */}
        <Modal
          animationType="fade"
          transparent={true}
          visible={showBranchesModal}
          onRequestClose={() => {
            setShowBranchesModal(!showBranchesModal);
          }}>
          <ModalWrapper>
            <ModalMainContainer>
              <ModalCloseIconWrapper
                onPress={() => setShowBranchesModal(false)}>
                <ModalLabelWrapper>
                  <ModalLabelText>Select a Branch</ModalLabelText>
                </ModalLabelWrapper>
                <CloseSVG width={25} height={25} />
              </ModalCloseIconWrapper>
              <ScrollView>
                {branchesList &&
                  branchesList.map((element, index) => {
                    return (
                      <ButtonWrapper
                        key={`branch${index}`}
                        isSelected={
                          selectedBranches
                            ? selectedBranches.id === element.id
                            : false
                        }
                        onPress={() => {
                          setSelectedBranches(element);
                          setShowBranchesModal(false);
                        }}>
                        <ButtonText
                          isSelected={
                            selectedBranches
                              ? selectedBranches.id === element.id
                              : false
                          }>
                          {element.name}
                        </ButtonText>
                      </ButtonWrapper>
                    );
                  })}
              </ScrollView>
            </ModalMainContainer>
          </ModalWrapper>
        </Modal>

        <ScrollView>
          <MainContainer>
            {/* Start Data Picker */}
            <SelectionItemContainer>
              <SelectionItemLabel>Start Date</SelectionItemLabel>
              <DatePicker
                mode={'date'}
                value={startDate}
                onChange={(e, d) => {
                  setStartDate(d);
                }}
              />
            </SelectionItemContainer>

            {/* End Data Picker */}
            <SelectionItemContainer>
              <SelectionItemLabel>End Date</SelectionItemLabel>
              <DatePicker
                mode={'date'}
                value={endDate}
                onChange={(e, d) => {
                  setEndDate(d);
                }}
              />
            </SelectionItemContainer>

            {/* Branch */}
            <SelectionItemContainer>
              <SelectionItemLabel>Branch</SelectionItemLabel>
              <BranchSelectorWrapper onPress={() => setShowBranchesModal(true)}>
                <Text>
                  {selectedBranches ? selectedBranches.name : 'Select Branch'}
                </Text>
              </BranchSelectorWrapper>
            </SelectionItemContainer>
            <SelectionItemContainer>

            <ConfirmationButtonWrapper onPress={() => submit()}>
              <BalanceButtonText isSelected={true}>Submit</BalanceButtonText>
            </ConfirmationButtonWrapper>
</SelectionItemContainer>
            <DetailContainer>
              {remmitanceData &&
                remmitanceData.map((element, index) => {
                  return (
                    index !== 0 && (
                      <DetailWrapper index={index} key={`Balance${index}`}>
                        <DetailView>
                          <StatusView>
                            {element.StatusID === '3' && (
                              <ClearedSVG width={25} height={25} fill="green" />
                            )}
                            {element.StatusID === '4' && (
                              <CancelSVG width={25} height={25} fill="red" />
                            )}
                            {element.StatusID !== '3' &&
                              element.StatusID !== '4' && (
                                <PendingSVG
                                  width={25}
                                  height={25}
                                  fill="#ebae34"
                                />
                              )}
                          </StatusView>
                          <NameDetail>
                            <NameText>{element.AgentName}</NameText>
                            <VrNoText>{element.vrnodisplay_s}</VrNoText>
                            <VrDateText>{element.VrDate}</VrDateText>
                          </NameDetail>
                          <AmountDetail>
                            <LCView>
                              <LCText>LC</LCText>
                              <AmountText>{` ${formatAmount(
                                element.AMT_LC,
                              )} `}</AmountText>
                              <CCYText>{element.LCY}</CCYText>
                            </LCView>

                            <FCView>
                              <FCText>FC</FCText>
                              <FCAmountText>{` ${formatAmount(
                                element.AMT_FC,
                              )} `}</FCAmountText>
                              <CCYText>{element.FCY}</CCYText>
                            </FCView>

                            <PayModeText>
                              PayMode: {element.PayMode}
                            </PayModeText>
                          </AmountDetail>
                        </DetailView>
                        <Details>
                          <DetailText>
                            Description: {element.Description}
                          </DetailText>
                        </Details>
                      </DetailWrapper>
                    )
                  );
                })}
              {remmitanceData && (
                <View style={{alignItems: 'flex-end', marginTop: 10}}>
                  <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                    Total LC: {remmitanceData[0].TotalLC}
                  </Text>
                  <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                    Total Transactions: {remmitanceData[0].nTotal}
                  </Text>
                </View>
              )}
            </DetailContainer>
            {/* <Text>
            {remmitanceData && JSON.stringify(remmitanceData[0].nTotal)}
          </Text> */}
          </MainContainer>
        </ScrollView>
      </ImageBackground>
    </Wrapper>
  );
};

const mapStateToProps = state => {
  return {USER_DETAIL: state.USER_DETAIL};
};

export default connect(mapStateToProps)(RemittanceInward);
