import React, {useEffect, useState} from 'react';
import {Modal, ScrollView, ImageBackground, Alert} from 'react-native';
import {connect} from 'react-redux';

import {CMB_CurrencyAPI, GetFCYLedgerAPI} from '../../Store/apiCalls';
import {
  MainNavigationHeader,
  DateTimePicker as DatePicker,
} from '../../Components';
import {Images} from '../../Theme';
import {CloseSVG} from '../../Theme/SVG';
import {dateFormat, formatAmount} from '../../Theme/utils';

import {
  Wrapper,
  MainContainer,
  SelectionItemContainer,
  SelectionItemLabel,
  ModalWrapper,
  ModalLabelWrapper,
  ModalLabelText,
  ButtonWrapper,
  ButtonText,
  ModalMainContainer,
  ModalCloseIconWrapper,
  BalanceButtonText,
  DetailContainer,
  DetailWrapper,
  DetailView,
  NameDetail,
  BalanceText,
  AmountDetail,
  LCView,
  AmountText,
  FCView,
  AmountLabel,
  VrDateText,
  DetailText,
  ConfirmationButtonWrapper,
  AccountInputField,
  CurrencyButtonWrapper,
  VoucherNoText,
  CurrencyHeading,
} from './style';

const FCYLedger = props => {
  const [showCurrencyModal, setShowCurrencyModal] = useState(false);
  const [selectedCurrency, setSelectedCurrency] = useState(null);
  const [currencyList, setCurrencyList] = useState([]);

  const [FCYLedgerData, setFCYLedgerData] = useState(null);
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [accountNumber, setAccountNumber] = useState(false);

  // Onfocus event handler
  useEffect(() => {
    setSelectedCurrency({
      id: 0,
      name: 'None',
    });
    const unsubscribe = props.navigation.addListener('focus', () => {
      CMB_CurrencyAPI(CMB_CurrencyAPIHandler);
    });

    return unsubscribe;
  }, [props.navigation, props.USER_DETAIL]);

  const CMB_CurrencyAPIHandler = res => {
    let obj = [{id: 0, name: 'None'}];
    res.responseBody.children[1].children[0].children.forEach(element => {
      obj.push({
        name: element.children[0].value,
        id: element.children[1].value,
      });
    });
    setCurrencyList(obj);
  };

  // submit Button
  const submit = () => {
    if (accountNumber) {
      console.log('error');
      let body = {
        CCYID: selectedCurrency.id,
        strDTto: dateFormat(endDate),
        strDTFrom: dateFormat(startDate),
        ACCNo: accountNumber,
      };
      GetFCYLedgerAPI(body, GetFCYLedgerAPIHandler);
    } else {
      Alert.alert('Please specify an account number first');
    }
  };

  const GetFCYLedgerAPIHandler = res => {
    let datalist = res.responseBody.children[1].children;

    let datalist2 = res.responseBody.children[0];
    console.log(JSON.stringify(datalist2));

    if (datalist.length > 0) {
      const data = res.responseBody.children[1].children[0].children;

      let objList = [];
      data.forEach(item => {
        let row = {};
        item.children.forEach(element => {
          row[element.name] = element.value;
        });
        objList.push(row);
      });

      setFCYLedgerData(objList);
    } else {
      Alert.alert('Alert', 'No data found');
    }
  };

  return (
    <Wrapper>
      <MainNavigationHeader HeaderText="FCY Ledger" />
      <ImageBackground
        source={Images.mainBackground}
        style={{width: '100%', flex: 1}}>
        {/* Currency Modal */}
        <Modal
          animationType="fade"
          transparent={true}
          visible={showCurrencyModal}
          onRequestClose={() => {
            setShowCurrencyModal(!showCurrencyModal);
          }}>
          <ModalWrapper>
            <ModalMainContainer>
              <ModalCloseIconWrapper
                onPress={() => setShowCurrencyModal(false)}>
                <ModalLabelWrapper>
                  <ModalLabelText>Select a Currency</ModalLabelText>
                </ModalLabelWrapper>
                <CloseSVG width={25} height={25} />
              </ModalCloseIconWrapper>
              <ScrollView>
                {currencyList &&
                  currencyList.map((element, index) => {
                    return (
                      <ButtonWrapper
                        key={`branch${index}`}
                        isSelected={
                          selectedCurrency
                            ? selectedCurrency.id === element.id
                            : false
                        }
                        onPress={() => {
                          setSelectedCurrency(element);
                          setShowCurrencyModal(false);
                        }}>
                        <ButtonText
                          isSelected={
                            selectedCurrency
                              ? selectedCurrency.id === element.id
                              : false
                          }>
                          {element.name}
                        </ButtonText>
                      </ButtonWrapper>
                    );
                  })}
              </ScrollView>
            </ModalMainContainer>
          </ModalWrapper>
        </Modal>

        <ScrollView>
          <MainContainer>
            {/* Start Data Picker */}
            <SelectionItemContainer>
              <SelectionItemLabel>Start Date</SelectionItemLabel>
              <DatePicker
                mode={'date'}
                value={startDate}
                onChange={(e, d) => {
                  setStartDate(d);
                }}
              />
            </SelectionItemContainer>

            {/* End Data Picker */}
            <SelectionItemContainer>
              <SelectionItemLabel>End Date</SelectionItemLabel>
              <DatePicker
                mode={'date'}
                value={endDate}
                onChange={(e, d) => {
                  setEndDate(d);
                }}
              />
            </SelectionItemContainer>

            {/* Currency Button */}
            <SelectionItemContainer>
              <SelectionItemLabel>Currency</SelectionItemLabel>
              <CurrencyButtonWrapper onPress={() => setShowCurrencyModal(true)}>
                <ButtonText>
                  {selectedCurrency ? selectedCurrency.name : 'Select Currency'}
                </ButtonText>
              </CurrencyButtonWrapper>
            </SelectionItemContainer>

            {/* Account Input field */}
            <SelectionItemContainer>
              <SelectionItemLabel>Account</SelectionItemLabel>
              <AccountInputField onChangeText={setAccountNumber} />
            </SelectionItemContainer>

            <ConfirmationButtonWrapper onPress={() => submit()}>
              <BalanceButtonText isSelected={true}>Submit</BalanceButtonText>
            </ConfirmationButtonWrapper>

            <DetailContainer>
              {FCYLedgerData &&
                FCYLedgerData.map((element, index) => {
                  return (
                    <DetailWrapper
                      isCurrency={element.GROUPID}
                      index={index}
                      key={`Balance${index}`}>
                      <DetailView isCurrency={element.GROUPID}>
                        <NameDetail>
                          {element.GROUPID ? (
                            <>
                              <CurrencyHeading>
                                {element.vrDate}
                              </CurrencyHeading>
                            </>
                          ) : (
                            <>
                              <VoucherNoText>
                                {element.vrnodisplay_s}
                              </VoucherNoText>
                              <VrDateText>{element.vrDate}</VrDateText>
                              <DetailText>{element.Details}</DetailText>
                            </>
                          )}
                        </NameDetail>
                        <AmountDetail>
                          <LCView>
                            <AmountLabel>Dr</AmountLabel>
                            <AmountText>{` ${formatAmount(
                              element.DR,
                            )}`}</AmountText>
                          </LCView>

                          <FCView>
                            <AmountLabel>Cr</AmountLabel>
                            <AmountText>{` ${formatAmount(
                              element.CR,
                            )}`}</AmountText>
                          </FCView>

                          <FCView>
                            <AmountLabel>Bal</AmountLabel>
                            <BalanceText amount={element.BAL}>
                              {formatAmount(element.BAL)}
                            </BalanceText>
                          </FCView>
                        </AmountDetail>
                      </DetailView>
                    </DetailWrapper>
                  );
                })}
            </DetailContainer>
          </MainContainer>
        </ScrollView>
      </ImageBackground>
    </Wrapper>
  );
};

const mapStateToProps = state => {
  return {USER_DETAIL: state.USER_DETAIL};
};

export default connect(mapStateToProps)(FCYLedger);
