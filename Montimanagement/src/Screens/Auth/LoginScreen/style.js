import styled from 'styled-components/native';
import {Colors} from '../../../Theme';

export const MainWrapper = styled.ImageBackground`
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

export const Wrapper = styled.View`
  width: 90%;
  min-height: 50%;
  padding-horizontal: 5%;
  border-radius: 10px;
  background-color: ${Colors.WHITE};
  padding-top: 30px;
  shadow-radius: 20px;

  shadow-opacity: 3;
  elevation: 15;
`;

export const TitleText = styled.Text`
  justify-content: center;
  color: ${Colors.MAIN_COLOR};
  font-size: 20px;
  font-weight: bold;
  align-self: center;
`;

export const TextButtonWrapper = styled.TouchableOpacity`
  align-items: flex-end;
  justify-content: center;
  padding-vertical: 5px;
  flex-direction: row;
`;

export const TextButtonText = styled.Text`
  color: ${Colors.MAIN_COLOR};
  font-size: 18px;
  font-weight: bold;
`;

export const LoginButtonWrapper = styled.TouchableOpacity`
  height: 50px;
  background-color: ${Colors.MAIN_COLOR};
  align-items: center;
  justify-content: center;
  flex-direction: row;
  margin-bottom: 20px;
  align-self: center;
  padding-horizontal: 20px;
  border-radius: 25px;
  width:80%;
`;

export const ButtonText = styled.Text`
  font-size: 15px;
  font-weight: bold;
  color: ${Colors.WHITE};
  margin-left: 10px;
`;

export const NormalText = styled.Text`
  font-size: 16px;
  color: ${Colors.BLACK};
`;

export const LogoImage1 = styled.Image`
  height: 150px;
  width: 150px;
  margin-right: 0px;
  margin-top: 20px;
  margin-bottom: 10px;
  resize-mode: stretch;
`;

export const LogoImage2 = styled.Image`
  height: 140px;
  width: 120px;
  margin-right: 60px;
  margin-top: 20px;
  margin-bottom: 10px;
  resize-mode: stretch;
`;
