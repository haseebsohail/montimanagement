/* eslint-disable no-alert */
import React, {useEffect, useState} from 'react';
import {Alert, BackHandler} from 'react-native';
import {connect} from 'react-redux';

import {AsyncStorage} from '../../../Theme/Libraries';

import {useTranslation} from '../../../LanguageContext';
import {Images, Colors} from '../../../Theme';
import {InputField} from '../../../Components';
import {LoginApi, getCustomerAPI} from '../../../Store/apiCalls';
import {TextV, TextValidator} from '../../../Theme/utils';
import {UserSVG, LoginSVG, PadlockSVG} from '../../../Theme/SVG';

import {
  MainWrapper,
  LogoImage1,
  LogoImage2,
  Wrapper,
  TitleText,
  LoginButtonWrapper,
  ButtonText,
  ButtonWrapper,
  TextButtonText,
  NormalText,
} from './style';

const LoginScreen = props => {
  const [userIpnut, setUserIpnut] = useState({});
  useEffect(() => {
    const backAction = () => {
      Alert.alert('Hold on!', 'Are you sure you want exit the app?', [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'YES', onPress: () => BackHandler.exitApp()},
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const loginResponseHandler = async res => {
    if (!res.success) {
      alert(`Network Error: check your internet connection and try again`);
    } else {
      let data = res.responseBody.children;
      if (data) {
        let USER_DETAIL = {};
        data.forEach(element => {
          USER_DETAIL[element.name] = element.value;
        });
       await AsyncStorage.setItem('@credientials',JSON.stringify(userIpnut));

        if (USER_DETAIL.isError === 'True') {
          alert(USER_DETAIL.Result);
        } else {
          props.dispatch({type: 'USER_DETAIL', USER_DETAIL});
          props.navigation.navigate('HomeScreen');
        }
      }
    }
  };

  const loginFunc = async () => {
    if (!userIpnut.userName || !userIpnut.password) {
      alert('Complete the form first');
    } else {
      LoginApi(userIpnut, loginResponseHandler);
    }
  };

  const textOnchange = (text, name) => {
    let user = userIpnut;
    user[name] = text;
    setUserIpnut(user);
  };

  return (
    <MainWrapper source={Images.loginBackground}>
      {/* user Input */}
      <Wrapper>
        {/* <TextInput
          value={props.route.params.token && props.route.params.token}
        /> */}

        <TitleText>LOGIN</TitleText>
        {/* <InputField
          disableFullscreenUI={true}
          autoCapitalize="none"
          secureTextEntry={false}
          placeholder={'Company Code'}
          onChangeText={text => textOnchange(text, 'companyCode')}
          icon={<UserSVG width={20} height={20} fill={Colors.MAIN_COLOR} />}
        /> */}

        <InputField
          disableFullscreenUI={true}
          autoCapitalize="none"
          secureTextEntry={false}
          placeholder={'Username'}
          onChangeText={text => textOnchange(text, 'userName')}
          icon={<UserSVG width={20} height={20} fill={Colors.MAIN_COLOR} />}
        />

        <InputField
          disableFullscreenUI={true}
          autoCapitalize="none"
          secureTextEntry={true}
          placeholder={'Password'}
          onChangeText={text => textOnchange(text, 'password')}
          icon={<PadlockSVG width={20} height={20} fill={Colors.MAIN_COLOR} />}
        />

        <LoginButtonWrapper onPress={() => loginFunc()}>
          <LoginSVG width={20} height={20} fill={Colors.white} />
          <ButtonText>Login</ButtonText>
        </LoginButtonWrapper>
      </Wrapper>
    </MainWrapper>
  );
};
const mapStateToProps = state => {
  return {USER_DETAIL: state.USER_DETAIL};
};
export default connect(mapStateToProps)(LoginScreen);
