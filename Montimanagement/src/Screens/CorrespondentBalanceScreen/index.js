import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Modal,
  ScrollView,
  TouchableOpacity,
  Platform,
  ImageBackground,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
// import DateTimePicker from '@react-native-community/datetimepicker';

import {
  CMB_AgentBranchesAPI,
  CMB_CurrencyAPI,
  GetCurrencyCorrespondentAPI,
} from '../../Store/apiCalls';
import {MainNavigationHeader, DateTimePicker} from '../../Components';
import Images from '../../Theme/Images';

import {CloseSVG} from '../../Theme/SVG';
import {dateFormat, formatAmount} from '../../Theme/utils';

import {
  Wrapper,
  Row,
  MainContainer,
  ModalWrapper,
  ModalLabelWrapper,
  ModalLabelText,
  ButtonWrapper,
  ButtonText,
  BalanceCorrespondent,
  ModalMainContainer,
  ModalCloseIconWrapper,
  BalancesButtonWrapper,
  BalanceButtonText,
  BalanceText,
  TotalBalanceText,
  TotalBalanceWrap,
  ConfirmationButtonWrapper,
  CurrecyWrapper,
} from './style';

const CorrespondentBalanceScreen = props => {
  const [showBranchesModal, setShowBranchesModal] = useState(false);
  const [showCurrencyModal, setShowCurrencyModal] = useState(false);
  const [branchesList, setBranchesList] = useState([]);
  const [currencyList, setCurrencyList] = useState([]);
  const [currencyCorrespondentList, setCurrencyCorrespondentList] =
    useState(null);
  const [currencyCorrespondentDetail, setCurrencyCorrespondentDetail] =
    useState(null);
  const [currencyBalanceDetail, setCurrencyBalanceDetail] = useState(null);
  const [selectedBranches, setSelectedBranches] = useState(null);
  const [selectedCurrency, setSelectedCurrency] = useState(null);

  const [selectedDate, setSelectedDate] = useState(new Date());

  useEffect(() => {
    // if (props.USER_DETAIL) {
    //   const obj = {
    //     id: props.USER_DETAIL.BranchID,
    //     name: props.USER_DETAIL.BranchName,
    //   };
    //   setSelectedBranches(obj);
    //   let agent_GUID = props.USER_DETAIL.CC_GUID;
    //   CMB_AgentBranchesAPI(agent_GUID, CMB_AgentBranchesAPIHandler);
    //   CMB_CurrencyAPI(CMB_CurrencyAPIHandler);
    // }
  }, []);

  // Onfocus event handler
  // React.useEffect(() => {
  //   const unsubscribe = props.navigation.addListener('focus', () => {
  //     let agent_GUID = props.USER_DETAIL.CC_GUID;
  //     CMB_AgentBranchesAPI(agent_GUID, CMB_AgentBranchesAPIHandler);
  //     CMB_CurrencyAPI(CMB_CurrencyAPIHandler);
  //   });
  //   return unsubscribe;
  // }, [props.navigation, props.USER_DETAIL]);

  const CMB_AgentBranchesAPIHandler = res => {
    console.log("rest");
    let obj = [{id: 0, name: 'None'}];
    res.responseBody.children[1].children[0].children.forEach(element => {
      obj.push({
        name: element.children[0].value,
        id: element.children[1].value,
      });
    });
    setBranchesList(obj);
  };

  const CMB_CurrencyAPIHandler = res => {
    let obj = [{id: 0, name: 'None'}];
    res.responseBody.children[1].children[0].children.forEach(element => {
      obj.push({
        name: element.children[0].value,
        id: element.children[1].value,
      });
    });
    setCurrencyList(obj);
  };

  // submit Button
  const submit = () => {
    setCurrencyCorrespondentList();
    let sc = {};
    if (!selectedCurrency) {
      sc = {id: 0, name: 'none'};
    } else {
      sc = selectedCurrency;
    }
    let body = {
      CCID: '0',
      CCYID: sc.id,
      strEnd: dateFormat(selectedDate),
    };
    GetCurrencyCorrespondentAPI(body, GetCurrencyCorrespondentAPIHandler);
  };

  const GetCurrencyCorrespondentAPIHandler = res => {
    let datalist = res.responseBody.children[1].children;

    if (datalist.length > 0) {
      const data = res.responseBody.children[1].children[0].children;

      let objList = [];
      data.forEach(item => {
        let row = {};
        item.children.forEach(element => {
          row[element.name] = element.value;
        });
        objList.push(row);
      });

      // Seprate Currency Data
      let cl = [];
      let newObj = {};
      objList.forEach(element => {
        if (newObj[element.CurrencyShortName]) {
          newObj[element.CurrencyShortName].push(element);
        } else {
          cl.push(element.CurrencyShortName);
          newObj[element.CurrencyShortName] = [element];
        }
      });

      let currencyBalance = {};
      cl.forEach(item => {
        let balance = 0;
        newObj[item].forEach(element => {
          balance = balance + (element.dr - element.cr);
        });
        currencyBalance[item] = balance;
      });

      setCurrencyBalanceDetail(currencyBalance);
      setCurrencyCorrespondentList(cl);
      setCurrencyCorrespondentDetail(newObj);
    } else {
      Alert.alert('Álert', 'No data found');
    }
  };

  return (
    <Wrapper>
      <MainNavigationHeader HeaderText="Correspondent Balance" />
      <ImageBackground
        source={Images.mainBackground}
        style={{width: '100%', flex: 1}}>
        {/* Currency Modal */}
        <Modal
          animationType="fade"
          transparent={true}
          visible={showCurrencyModal}
          onRequestClose={() => {
            setShowCurrencyModal(!showCurrencyModal);
          }}>
          <ModalWrapper>
            <ModalMainContainer>
              <ModalCloseIconWrapper
                onPress={() => setShowCurrencyModal(false)}>
                <ModalLabelWrapper>
                  <ModalLabelText>Select a Currency</ModalLabelText>
                </ModalLabelWrapper>
                <CloseSVG width={25} height={25} />
              </ModalCloseIconWrapper>
              <ScrollView>
                {currencyList &&
                  currencyList.map((element, index) => {
                    return (
                      <ButtonWrapper
                        key={`branch${index}`}
                        isSelected={
                          selectedCurrency
                            ? selectedCurrency.id === element.id
                            : false
                        }
                        onPress={() => {
                          setSelectedCurrency(element);
                          setShowCurrencyModal(false);
                        }}>
                        <ButtonText
                          isSelected={
                            selectedCurrency
                              ? selectedCurrency.id === element.id
                              : false
                          }>
                          {element.name}
                        </ButtonText>
                      </ButtonWrapper>
                    );
                  })}
              </ScrollView>
            </ModalMainContainer>
          </ModalWrapper>
        </Modal>
        <ScrollView>
          <MainContainer>
            {/* Currency Button */}
            <ButtonWrapper onPress={() => setShowCurrencyModal(true)}>
              <ButtonText>
                {selectedCurrency ? selectedCurrency.name : 'Select Currency'}
              </ButtonText>
            </ButtonWrapper>

            {/* Date Picker */}
            <View style={{margin: 10}}>
              <DateTimePicker
                mode={'date'}
                value={selectedDate}
                onChange={(e, d) => {
                  setSelectedDate(d);
                }}
              />
            </View>

            {currencyCorrespondentList && (
              <Row>
                <BalancesButtonWrapper isSelected key={'BalanceHeader'}>
                  <BalanceCorrespondent isSelected>
                    Correspondent
                  </BalanceCorrespondent>
                  <BalanceText isSelected> Balance</BalanceText>
                </BalancesButtonWrapper>
                {currencyCorrespondentList.map((item, ind) => {
                  return (
                    <View key={`data${ind}`}>
                      <CurrecyWrapper key={`Currency${ind}`}>
                        <Text>{item}</Text>
                      </CurrecyWrapper>
                      {currencyCorrespondentDetail &&
                        currencyCorrespondentDetail[item] &&
                        currencyCorrespondentDetail[item].map(
                          (element, index) => {
                            return (
                              <BalancesButtonWrapper key={`Balance${index}`}>
                                <BalanceCorrespondent>
                                  {`${element.dtcd_Prt1}-${element.sBcd_Prt1}     ${element.SbNa_Prt}`}
                                </BalanceCorrespondent>
                                <BalanceText>
                                  {formatAmount(element.dr - element.cr)}
                                </BalanceText>
                              </BalancesButtonWrapper>
                            );
                          },
                        )}
                      <TotalBalanceWrap key={`TotalBalance${ind}`}>
                        <TotalBalanceText>
                          {`Total Balance   ${
                            currencyBalanceDetail && currencyBalanceDetail[item]
                          }`}
                        </TotalBalanceText>
                      </TotalBalanceWrap>
                    </View>
                  );
                })}
              </Row>
            )}

            <ConfirmationButtonWrapper onPress={() => submit()}>
              <BalanceButtonText isSelected={true}>Submit</BalanceButtonText>
            </ConfirmationButtonWrapper>
          </MainContainer>
        </ScrollView>
      </ImageBackground>
    </Wrapper>
  );
};

const mapStateToProps = state => {
  return {USER_DETAIL: state.USER_DETAIL};
};

export default connect(mapStateToProps)(CorrespondentBalanceScreen);
