import styled from 'styled-components/native';

import {Colors} from '../../Theme';
import colors from '../../Theme/Colors';

export const Wrapper = styled.View`
  background-color: ${Colors.WHITE};
  flex: 1;
`;

export const Row = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  margin-horizontal: 5%;
  background-color: #e6e6e6;
  border-radius: 10px;
  padding-bottom: 20px;
`;

export const DateSeletorContainer = styled.View`
  flex-direction: row;
  justify-content: center;
  margin-vertical: 20px;
`;

export const MainContainer = styled.View`
  flex: 1;
  margin-top: 50px;
  align-items: center;
`;

export const ModalWrapper = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1;
  background-color: rgba(0, 0, 0, 0.5);
`;

export const ModalLabelWrapper = styled.View`
  align-items: center;
  flex: 1;
`;

export const ModalLabelText = styled.Text`
  color: ${Colors.MAIN_COLOR};
  font-size: 16px;
  font-weight: 600;
`;

export const ModalCloseIconWrapper = styled.TouchableOpacity`
  margin: 10px;
  flex-direction: row;
`;

export const ModalMainContainer = styled.View`
  width: 90%;
  border-radius: 10px;
  padding-bottom: 20px;
  border-color: rgba(0, 0, 0, 0.2);
  background-color: white;
  border-width: 1px;
  border-radius: 10px;
  min-height: 40%;
  max-height: 90%;
`;

export const ButtonWrapper = styled.TouchableOpacity`
  width: 90%;
  margin-top: 5px;
  border-radius: 5px;
  background-color: ${props =>
    props.isSelected ? colors.MAIN_COLOR : 'white'};
  flex-direction: column;
  justify-content: center;
  align-items: center;
  align-self: center;
  padding: 10px;

  shadow-color: #000;
  shadow-opacity: 0.3;
  shadow-radius: 5px;
  elevation: 14;
`;

export const BalanceContainerLabel = styled.View`
  width: 100%;
  height: 50px;
  align-items: center;
  justify-content: center;
`;

export const BalancesButtonWrapper = styled.View`
  flex: 1px;
  margin-horizontal: 2%;
  margin-top: 2px;
  background-color: ${props =>
    props.isSelected ? Colors.MAIN_COLOR : 'white'};
  flex-direction: row;
  justify-content: space-between;
  padding: 10px;
  shadow-color: #000;
  shadow-opacity: 0.3;
  shadow-radius: 5px;
  elevation: 14;
`;

export const CurrecyWrapper = styled.View`
  flex: 1px;
  margin-horizontal: 2%;
  margin-top: 2px;
  background-color: ${props => (props.isSelected ? Colors.MAIN_COLOR : 'grey')};
  flex-direction: row;
  justify-content: space-between;
  padding: 10px;
  shadow-color: #000;
  shadow-opacity: 0.3;
  shadow-radius: 5px;
  elevation: 14;
`;

export const TotalBalanceWrap = styled.View`
  flex: 1px;
  margin-horizontal: 2%;
  margin-top: 2px;
  flex-direction: row;
  justify-content: space-between;
  padding: 10px;
  shadow-color: #000;
  shadow-opacity: 0.3;
  shadow-radius: 5px;
  elevation: 14;
`;

export const TotalBalanceText = styled.Text`
  font-size: 16px;
  font-weight: 600;
  text-align: right;
  width: 100%;
`;

export const BookedBalancesButtonWrapper = styled.View`
  width: 30%;
  margin-left: 3%;
  margin-top: 10px;
  border-radius: 5px;
  background-color: ${Colors.themeGrey};
  flex-direction: column;
  justify-content: center;
  align-items: center;
  align-self: center;
  padding: 10px;

  shadow-color: #000;
  shadow-opacity: 0.3;
  shadow-radius: 5px;
  elevation: 14;
`;

export const BalanceButtonText = styled.Text`
  color: ${props => (props.isSelected ? Colors.white : Colors.themeGrey)};
  font-weight: 600;
`;

export const BalanceText = styled.Text`
  color: ${props => (props.isSelected ? Colors.white : Colors.themeGrey)};
  font-weight: 600;
`;

export const BalanceCorrespondent = styled.Text`
  color: ${props => (props.isSelected ? Colors.white : Colors.themeGrey)};
  font-weight: 600;
  width: 70%;
`;

export const ButtonText = styled.Text`
  color: ${props => (props.isSelected ? Colors.white : Colors.themeGrey)};
  font-weight: 600;
`;

export const ConfirmationButtonWrapper = styled.TouchableOpacity`
  width: 90%;
  height: 50px;
  margin-bottom: 20px;
  margin-top: 20px;
  border-radius: 5px;
  background-color: ${Colors.MAIN_COLOR};
  flex-direction: column;
  justify-content: center;
  align-items: center;
  align-self: center;
  padding: 10px;

  shadow-color: #000;
  shadow-opacity: 0.3;
  shadow-radius: 5px;
  elevation: 14;
`;
