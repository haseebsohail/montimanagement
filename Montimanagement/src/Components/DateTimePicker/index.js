import React, {useState} from 'react';

import DateTimePicker from '@react-native-community/datetimepicker';
import {Text, Platform, TouchableOpacity} from 'react-native';
import {dateFormat} from '../../Theme/utils';

const TimePicker = ({...props}) => {
  const [show, setShow] = useState(false);

  return (
    <>
      {Platform.OS === 'ios' ? (
        <DateTimePicker
          testID="dateTimePicker"
          value={props.value}
          mode={props.mode}
          is24Hour={true}
          display="inline"
          // eslint-disable-next-line react-native/no-inline-styles
          style={{
            width: '100%',
          }}
          onChange={props.onChange}
        />
      ) : (
        <>
          {show && (
            <DateTimePicker
              testID="dateTimePicker"
              value={props.value}
              is24Hour={true}
              mode={props.mode}
              display="default"
              // eslint-disable-next-line react-native/no-inline-styles
              style={{
                width: '100%',
              }}
              onChange={(e, d) => {
                if (e.type === 'dismissed') {
                  setShow(false);
                } else {
                  props.onChange(e, d);
                  setShow(false);
                }
              }}
            />
          )}

          <TouchableOpacity
            // eslint-disable-next-line react-native/no-inline-styles
            style={{
              justifyContent: 'center',
              padding: 10,
              borderRadius: 5,
              backgroundColor: '#ededed',
            }}
            onPress={() => setShow(true)}>
            <Text>{props.value ? dateFormat(props.value) : 'Select Date'}</Text>
          </TouchableOpacity>
        </>
      )}
    </>
  );
};

export default TimePicker;
