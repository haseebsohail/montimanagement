import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {Provider} from 'react-redux';
import {Store} from './Store';
import {
  LogoutSVG,
  Home,
  LoginSVG,
  UserSVG,
  DollorBagSVG,
  FinancialAccountSVG,
  FinancialProfitSVG,
  InSVG,
  OutSVG,
} from './Theme/SVG';
import {Colors} from './Theme';

const Tab = createMaterialBottomTabNavigator();

import {
  HomeScreen,
  LoginScreen,
  SplashScreen,
  CorrespondentBalanceScreen,
  CurrencyStock,
  CurrencyExposure,
  FCYLedger,
  RemittanceOutward,
  RemittanceInward,
} from './Screens';

function MyBottomTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      activeColor={Colors.MAIN_COLOR}
      inactiveColor="#919191"
      barStyle={{backgroundColor: '#FFF', height: 70}}
      labeled={false}>
      <Tab.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{
          tabBarIcon: ({color}) => (
            <Home fill={color} stroke={color} width={25} height={35} />
          ),
        }}
      />
      <Tab.Screen
        name="CorrespondentBalance"
        component={CorrespondentBalanceScreen}
        options={{
          tabBarIcon: ({color}) => (
            <UserSVG fill={color} stroke={color} width={25} height={35} />
          ),
        }}
      />
      <Tab.Screen
        name="CurrencyStock"
        component={CurrencyStock}
        options={{
          tabBarIcon: ({color}) => (
            <DollorBagSVG fill={color} stroke={color} width={25} height={35} />
          ),
        }}
      />
      <Tab.Screen
        name="CurrencyExposure"
        component={CurrencyExposure}
        options={{
          tabBarIcon: ({color}) => (
            <FinancialProfitSVG
              fill={color}
              stroke={color}
              width={25}
              height={35}
            />
          ),
        }}
      />
      <Tab.Screen
        name="FCYLedger"
        component={FCYLedger}
        options={{
          tabBarIcon: ({color}) => (
            <FinancialAccountSVG
              fill={color}
              stroke={color}
              width={25}
              height={35}
            />
          ),
        }}
      />
      <Tab.Screen
        name="RemittanceOutward"
        component={RemittanceOutward}
        options={{
          tabBarIcon: ({color}) => (
            <OutSVG fill={color} stroke={color} width={25} height={35} />
          ),
        }}
      />
      <Tab.Screen
        name="RemittanceInward"
        component={RemittanceInward}
        options={{
          tabBarIcon: ({color}) => (
            <InSVG fill={color} stroke={color} width={25} height={35} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const Stack = createStackNavigator();

function App() {
  return (
    <Provider store={Store}>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
            gestureEnabled: false,
          }}>
          <Stack.Screen name="SplashScreen" component={SplashScreen} />

          <Stack.Screen name="HomeScreen" component={MyBottomTabs} />
          <Stack.Screen name="LoginScreen" component={LoginScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export default App;
