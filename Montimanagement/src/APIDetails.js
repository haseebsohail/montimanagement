// (((FOR BRANCHES)))

// <?xml version="1.0" encoding="utf-8"?>
// <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
//   <soap:Body>
//     <CMB_AgentBranches xmlns="http://tempuri.org/">
//       <CC_GUID>string</CC_GUID>
//     </CMB_AgentBranches>
//   </soap:Body>
// </soap:Envelope>

// -----------------------------------------------------------
// (((FOR CURRENCY )))

// <?xml version="1.0" encoding="utf-8"?>
// <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
//   <soap:Body>
//     <CMB_Currency xmlns="http://tempuri.org/" />
//   </soap:Body>
// </soap:Envelope>

// ----------------------------------
// (((fnGetCurrencyCorrespondent)))

// <?xml version="1.0" encoding="utf-8"?>
// <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
//   <soap:Body>
//     <fnGetCurrencyCorrespondent xmlns="http://tempuri.org/">
//       <CCID>int</CCID>
//       <CCYID>int</CCYID>
//       <strEnd>string</strEnd>
//     </fnGetCurrencyCorrespondent>
//   </soap:Body>
// </soap:Envelope>

// ----------------------------------------
// (((fnGetCurrencyExposure)))

// <?xml version="1.0" encoding="utf-8"?>
// <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
//   <soap:Body>
//     <fnGetCurrencyExposure xmlns="http://tempuri.org/">
//       <CCID>int</CCID>
//       <strEnd>string</strEnd>
//     </fnGetCurrencyExposure>
//   </soap:Body>
// </soap:Envelope>

// ----------------------------------------------------
// (((fnGetCurrencyStock)))

// <?xml version="1.0" encoding="utf-8"?>
// <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
//   <soap:Body>
//     <fnGetCurrencyStock xmlns="http://tempuri.org/">
//       <CCID>int</CCID>
//       <strEnd>string</strEnd>
//     </fnGetCurrencyStock>
//   </soap:Body>
// </soap:Envelope>

// -------------------------------------------------
// (((fnGetFCYLedger)))

// <?xml version="1.0" encoding="utf-8"?>
// <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
//   <soap:Body>
//     <fnGetFCYLedger xmlns="http://tempuri.org/">
//       <CCYID>int</CCYID>
//       <strDTFrom>string</strDTFrom>
//       <strDTto>string</strDTto>
//       <ACCNo>string</ACCNo>
//     </fnGetFCYLedger>
//   </soap:Body>
// </soap:Envelope>

// ---------------------------------------------

// Login APi response JSON
let res = {
  BranchID: '1',
  BranchName: 'CORPORATE OFFICE',
  CC_GUID: '5839077C-3CE4-4281-BE94-4338B616618C',
  Result: 'SUCCESS,Record Found,',
  UserGUID: '30F32E6D-FF59-4A90-B1B1-B01D9E9A103B',
  UserName: 'admin_CO',
  isError: 'False',
};

//
let body = [
  {id: '3', name: 'ABU DHABI'},
  {id: '6', name: 'ALAIN BR'},
  {id: '1', name: 'CORPORATE OFFICE'},
  {id: '5', name: 'DUBAI BR'},
  {id: '2', name: 'MAIN BRANCH'},
  {id: '4', name: 'MUSSAFFAH'},
];
