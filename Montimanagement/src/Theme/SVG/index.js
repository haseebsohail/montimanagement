import Home from './Home.svg';
import Paws from './Paws.svg';
import Notification from './Notification.svg';

import Calendar from './Calendar.svg';

import CashWidthDrawel from './cashWidthDrawel.svg';
import CashDeposit from './cashDeposit.svg';
import WPSSVG from './w_p_s.svg';
import OnlineTracSVG from './fingerprint.svg';
import LoginSVG from './loginSVG.svg';
import DocVerificationSVG from './docVerification.svg';
import BillPaymentsSVG from './billPayments.svg';
import InvestmentSVG from './investment.svg';
import OthersSVG from './others.svg';
import CloseSVG from './close.svg';
import LogoutSVG from './logout.svg';
import PhoneSVG from './phone.svg';
import EmailSVG from './email.svg';
import UserSVG from './user.svg';
import PadlockSVG from './padlock.svg';
import DollorBagSVG from './dollorBag.svg';
import FinancialProfitSVG from './financialProfit.svg';
import FinancialAccountSVG from './financialAccount.svg';
import BackSVG from './back.svg';
import InSVG from './in.svg';
import OutSVG from './out.svg';
import ClearedSVG from './cleared.svg';
import CancelSVG from './cancel.svg';
import PendingSVG from './pending.svg';

export {
  Home,
  Paws,
  Calendar,
  Notification,
  CashWidthDrawel,
  LoginSVG,
  CashDeposit,
  WPSSVG,
  OnlineTracSVG,
  DocVerificationSVG,
  BillPaymentsSVG,
  OthersSVG,
  InvestmentSVG,
  CloseSVG,
  LogoutSVG,
  PhoneSVG,
  EmailSVG,
  UserSVG,
  PadlockSVG,
  DollorBagSVG,
  FinancialProfitSVG,
  FinancialAccountSVG,
  BackSVG,
  InSVG,
  OutSVG,
  ClearedSVG,
  CancelSVG,
  PendingSVG,
};
