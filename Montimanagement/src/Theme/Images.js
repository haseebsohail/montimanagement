const images = {
  SPLASH_LOGO: require('./Images/SplashLogo.png'),
  Logo: require('./Images/Logo.png'),

  PASSWORD: require('./Images/Icons/Password.png'),
  USERNAME: require('./Images/Icons/UserName.png'),
  pictures: require('./Images/Icons/pictures.png'),

  WelcomeImage1: require('./Images/WelcomeImage1.png'),
  WelcomeImage2: require('./Images/WelcomeImage2.png'),
  WelcomeImage3: require('./Images/WelcomeImage3.png'),
  Email: require('./Images/Email.png'),
  PawtaiPaws: require('./Images/PawtaiPaws.png'),

  // Qtech
  loginBackground: require('./Images/loginBackground.png'),
  homeBackground: require('./Images/homebg.png'),
  mainBackground: require('./Images/mainbg.png'),
  backIcon: require('./Images/backIcon.png'),
  calendarIcon: require('./Images/calendarIcon.png'),
  qrCodeIcon: require('./Images/qrCodeIcon.png'),
  deleteIcon: require('./Images/deleteIcon.png'),
  updateIcon: require('./Images/updateIcon.png'),
};

export default images;
